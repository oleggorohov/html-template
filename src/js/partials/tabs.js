class TabList {
  constructor(tabsContainer, items) {
    this.tabsContainer = tabsContainer;
    this.items = items;

    const tabs = tabsContainer.querySelectorAll('.js-tab');
    for (var i = 0; i < tabs.length; i++) {
      let el = tabs[i];
      el.addEventListener('click', event => {
        const index = event.target.dataset.tab;
        this.openTab(index);
      });
    }
  }

  openTab(index) {
    this.tabsContainer.querySelector('.-active').classList.remove('-active');
    this.tabsContainer.querySelector(`div[data-tab='${index}']`).classList.add('-active');
    this.items.querySelector('.-active').classList.remove('-active');
    this.items.querySelector(`div[data-item='${index}']`).classList.add('-active');
  }
}

function initTabs() {
  const tabsContainer = document.querySelector('.js-tabs');
  if (tabsContainer) {
    const items = document.querySelector('.js-tab-items');

    const tabList = new TabList(tabsContainer, items);
  }
}

document.addEventListener('DOMContentLoaded', () => {
  initTabs();
})